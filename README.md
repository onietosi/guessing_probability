# Calculating the guessing probability with semi-definite programming

**Author**: Olmo Nieto Silleras

## Overview

We work in the framework of [Bell tests](https://en.wikipedia.org/wiki/Bell_test_experiments), in which two or more distant parties share a quantum system on which they can perform measurements.
Such a test is characterised by a probability distribution or _behaviour_ $`p`$ consisting of the conditional probabilities of obtaining certain outputs given that the parties performed certain measurements,

```math
p \equiv \{ p(a|x)\}
```

where $`x`$ and $`a`$ respectively denote the measurements performed by the parties and the outcome obtained after peforming these measurements, e.g. for two parties $`a=(a_1,a_2)`$ and $`x=(x_1,x_2)`$.

The **guessing probability** $`G(p)`$ is linked to the (quantum) [min-entropy](https://en.wikipedia.org/wiki/Min-entropy) through $`H_\text{min}(p) = -\log_2 G(p)`$ and is thus a measure of the amount of randomness present in the outcomes of a Bell test characterised by the behaviour $`p`$ (the lower the guessing probability, the higher the randomness in the outcomes) [1].
It is thus a quantity of interest in the context of [quantum random number generation](https://www.wired.com/story/quantum-mechanics-could-solve-cryptographys-random-number-problem/).

Mathematically, the guessing probability can be expressed as a [conic optimisation problem](https://en.wikipedia.org/wiki/Conic_optimization) over the set of allowed quantum behaviours $`\mathcal Q \equiv\{p\}`$ [2].
However, there is no practical characterisation of this set (e.g. in terms of a finite number of conditions) and there is thus no algorithm allowing to solve the problem exactly.
We nevertheless show that one can use the Navascués-Pironio-Acín (NPA) hierarchy to relax the problem to a hierarchy of semi-definite programs (SDP) [2, 3].
Such SDPs can then be numerically solved at a given level of the hierarchy, providing upper bounds on the guessing probability.

Here we provide a script to numerically calculate the guessing probability for any quantum behaviour in a bipartite, two-inputs per party, two-outputs per input Bell scenario, at level $`l=2`$ of the NPA hierarchy.
(Note that here we actually solve the dual formulation of the original optimisation problem, which is equivalent.
For this we use the dual of the NPA hierarchy, which can be formulated in terms of [sum-of-squares (SOS) decompositions](https://en.wikipedia.org/wiki/Polynomial_SOS) [2, 4].)

## Some important technicalities to understand our script

### Bell expressions

In the context of Bell tests and quantum information theory the concept of _Bell expression_ and its corresponding _Bell expectation value_ are of particular importance.
A Bell expression $`f`$ is simply a linear form in the space spanned by the set of behaviours $`\mathcal Q`$,

```math
f: \mathcal Q \rightarrow \mathbb R .
```

The expectation value $`I \equiv f[p]`$ can be used to determine whether the underlying system exhibits quantum properties (such as [nonlocality](https://en.wikipedia.org/wiki/Quantum_nonlocality) or [entanglement](https://en.wikipedia.org/wiki/Quantum_entanglement)) and is sometimes referred to as the _Bell violation_.

### Bounds on the guessing probability given the Bell expectation value

In previous works, it was shown that given the Bell expectation value, one can formulate and calculate upper bounds on the guessing probability

```math
G(p) \leq G(I) .
```

However, the Bell expectation value only contains partial information about the full behaviour $`p`$.
In our work, we show how to use all the information available in the behaviour to calculate the guessing probability itself, and not just bounds on this quantity [2].

### A general formulation given an arbitrary number of Bell expressions

The guessing probability $`G(p)`$ and the bounds $`G(I)`$ are in fact particular cases of a more general unified formulation.
Let $`\mathbf f`$ denote a vector of $`t`$ Bell expressions and $`\mathbf I = \mathbf f[p]`$ the corresponding vector of Bell expectations.
One can then formulate bounds of the form

```math
G(p) \leq G(\mathbf I) ,
```

of which the guessing probability and the guessing bound $`G(I)`$ are just particular cases.
Indeed, $`G(I)`$ just corresponds to the case $`t = 1`$ for a particular Bell expression $`f`$, while the guessing probability $`G(p)`$ corresponds to the vector $`\mathbf f`$ of Bell expressions $`f_{a,x}`$, where $`f_{a,x}(a', x') = \delta_{a,a'} \delta_{x,x'}`$, so that

```math
f_{a,x}[p] = \sum_{a',x'} f_{a,x}(a',x') p(a'|x') = p(a|x)
```

and $`\mathbf f[p] = p`$.

### The correlator representation

In the context of Bell tests, behaviours must satisfy certain mathematical constraints.
For example, for any given measurement setting, the probabilities should sum to one,

```math
\sum_a p(a|x) = 1 \quad \forall x .
```

This means that the components of a behaviour are not all independent and can thus be parametrised in terms of a lower number of independent parameters.
A particularly common parametrisation for Bell scenarios with dichotomic measurement outcomes is the _correlator representation_.
This parametrisation is defined in terms of the conditional probabilities by the following expectation values:

```math
\begin{aligned}
\langle A_{x_1} \rangle & = \sum_{a_1 \in \{1,-1\}} a_1 \, p(a_1|x_1), \\
\langle B_{x_2} \rangle & = \sum_{a_2 \in \{1,-1\}} a_2 \, p(a_2|x_2), \\
\langle A_{x_1}B_{x_2} \rangle & = \sum_{a_1,a_2 \in \{1,-1\}} a_1 a_2 \, p(a_1 a_2|x_1 x_2).
\end{aligned}
```

The full set of conditional probabilities $`p(a|x)`$ can be obtained from the above expectation values through the inverse transformation

```math
p(a_1 a_2 | x_1 x_2) = \frac{1}{4} (1 + a_1 \langle A_{x1} \rangle + a_2 \langle B_{x_2} \rangle + a_1 a_2 \langle A_{x_1} B_{x_2} \rangle),
```

with $`a_1, a_2 \in \{1,-1\}`$.

It is this representation that we use in our script, as it translates into a lower number of constraints when writing the code for the full optimisation problem.

## Running the script

**Requirements**: we use [YALMIP](https://yalmip.github.io/) (a toolbox for modeling and optimization in MATLAB) and [SeDuMi](http://sedumi.ie.lehigh.edu/) to define and numerically solve the SDP.

## References

[1] R. Konig, R. Renner and C. Schaffner, The Operational Meaning of Min- and Max-Entropy, [_IEEE Transactions on Information Theory_ **55**, 4337](https://doi.org/10.1109/TIT.2009.2025545), 2009.

[2] O. Nieto-Silleras, S. Pironio and J. Silman, Using complete measurement statistics for optimal device-independent randomness evaluation, [_New Journal of Physics_ **16**, 013035](https://doi.org/10.1088/1367-2630/16/1/013035), 2014.

[3] M. Navascués, S. Pironio and A. Acín, A convergent hierarchy of semidefinite programs characterizing the set of quantum correlations, [_New Journal of Physics_ **10**, 073013](https://doi.org/10.1088/1367-2630/10/7/073013), 2008.

[4] S. Pironio, M. Navascués and A. Acín, Convergent relaxations of polynomial optimization problems with noncommuting variables, [_SIAM Journal on Optimization_
**20**, 2157](https://doi.org/10.1137/090760155), 2010.

See also **sections 4.2** and **5.3** of my [PhD thesis](https://dipot.ulb.ac.be/dspace/bitstream/2013/271365/3/ONietoSilleras-thesis.pdf) and the [Ncpol2sdpa examples section](https://ncpol2sdpa.readthedocs.io/en/stable/exampleshtml.html#example-7-using-the-nieto-silleras-hierarchy).

Here is an accessible [article](https://www.wired.com/story/quantum-mechanics-could-solve-cryptographys-random-number-problem/) explaining the context in which our work is placed and to which it contributes.