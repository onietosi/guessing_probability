function constraints = define_constraints(d,inputs)
% DEFINE_CONSTRAINTS    Defines guessing probability constraints at level 2
%                       of the SOS hierarchy (dual of NPA)

% Declaration of optimisation variables
M=sdpvar(13,13);
N=sdpvar(13,13);

constraints=[];

alice_flag=inputs(1) && 1;
bob_flag=inputs(2) && 1;
global_flag = alice_flag*bob_flag;
x=inputs(1);
y=inputs(2);

    
af1=alice_flag*mod(x,2); % Takes value 1 if x=1, 0 if x=2, 0 if x=0
af2=alice_flag*mod(x+1,2); % Takes value 0 if x=1, 1 if x=2, 0 if x=0
bf1=bob_flag*mod(y,2); % Takes value 1 if y=1, 0 if y=2, 0 if y=0
bf2=bob_flag*mod(y+1,2); % Takes value 0 if y=1, 1 if y=2, 0 if y=0
gf=1/2*1/(global_flag+1); % Takes value 1/2 or 1/4 respectively for local or global guessing probability

% Assignment of constraints
    % Equating terms proportional to AixBj in the SOS2 decomposition (a=1 or b=1 or a=b=1)
    
    lhs1 = d + [-gf,...% IxI
                -gf*af1,...% A1xI
                -gf*af2,...% A2xI
                -gf*bf1,...% IxB1
                -gf*bf2,...% IxB2
                -gf*af1*bf1,...% A1xB1
                -gf*af1*bf2,...% A1xB2
                -gf*af2*bf1,...% A2xB1
                -gf*af2*bf2]; % A2xB2
    
    % Equating terms proportional to AixBj in the SOS2 decomposition (a=-1 or b=-1 or a=b=-1)
    lhs2 = d + [-gf, ...% IxI
                +gf*af1,...% A1xI
                +gf*af2,...% A2xI
                +gf*bf1,...% IxB1
                +gf*bf2,...% IxB2
                -gf*af1*bf1,...% A1xB1
                -gf*af1*bf2,...% A1xB2
                -gf*af2*bf1,...% A2xB1
                -gf*af2*bf2]; % A2xB2

    constraints=constraints+sos_constraints(lhs1,M);    
    constraints=constraints+sos_constraints(lhs2,N);

    if global_flag == 1

        O=sdpvar(13,13);
        P=sdpvar(13,13);
        % Equating terms proportional to AixBj in the SOS2 decomposition (a=-1,b=1)
        lhs3 = d + [-gf, ... % IxI
                    +gf*af1,...% A1xI 
                    +gf*af2,...% A2xI
                    -gf*bf1,...% IxB1
                    -gf*bf2,...% IxB2
                    +gf*af1*bf1,...% A1xB1 
                    +gf*af1*bf2,...% A1xB2
                    +gf*af2*bf1,...% A2xB1
                    +gf*af2*bf2]; % A2xB2
        
        % Equating terms proportional to AixBj in the SOS2 decomposition (a=1,b=-1)        
        lhs4 = d + [-gf, ... % IxI
                    -gf*af1,...% A1xI
                    -gf*af2,...% A2xI
                    +gf*bf1,...% IxB1
                    +gf*bf2,...% IxB2
                    +gf*af1*bf1,...% A1xB1
                    +gf*af1*bf2,...% A1xB2
                    +gf*af2*bf1,...% A2xB1
                    +gf*af2*bf2]; % A2xB2

        constraints=constraints+sos_constraints(lhs3,O);
        constraints=constraints+sos_constraints(lhs4,P);
    end
    
end