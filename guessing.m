function [pguess,opt_coeffs] = guessing(coeffs,ev,inputs)
% GUESSING      Calculates the guessing probability and optimal Bell 
%               coefficients in a (2,2,2) Bell scenario for a pair of INPUTS 
%               and given a violation COEFFS*EV
%

n_bell = size(coeffs,1);
n_r = size(inputs,1);

yalmip clear
d = sdpvar(1,9);
y0 = sdpvar(1);
y = sdpvar(1,n_bell);

ev_par = sdpvar(n_bell,1);

constraints = [];

constraints = constraints + [d == y0*eye(1,9) + y*coeffs];
% Define SOS constraints for each randomness-generating input
for i=1:n_r
    constraints = constraints + define_constraints(d,inputs(i,:));
end

% Define objective function
obj = y0 + y*ev_par;

% Solve optimisation
opt = optimizer(constraints,obj,sdpsettings('solver','sedumi', 'verbose',0),...
    ev_par,{obj,[y0;y(:)]});
soln = opt{ev};

pguess=soln{1};
opt_coeffs=soln{2};
    
end